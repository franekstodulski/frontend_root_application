import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { RouterModule } from '@angular/router';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

import { AppComponent } from './app.component';
// Application routes
import { routes } from './routes';

import { NgxsModule } from '@ngxs/store';
import { UserState } from './store/user.state';

// Views
import { ViewsModule } from './views/views.module';

@NgModule({
  declarations: [AppComponent],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    ViewsModule,
    RouterModule.forRoot(routes),
    NgxsModule.forRoot([UserState])
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule {}
