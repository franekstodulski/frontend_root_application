import { Component, OnInit, OnDestroy } from '@angular/core';
import { Router } from '@angular/router';

// Forms
import { FormGroup, FormControl, Validators } from '@angular/forms';

// Interfaces
import { LoginForm } from './login-form';

// RxJS
import { Subscription } from 'rxjs';

// Encrypting
import * as sha1 from 'sha1/sha1';

// NGXS
import { Store } from '@ngxs/store';
import { AddUser } from '../../store/user.actions';

// Services
import { RootAdminService } from 'src/app/services/root-user/root-user.service';

// JWT
import { JwtAuthService } from '../../services/jwt-auth/jwt-auth.service';

@Component({
  selector: 'app-login-form',
  templateUrl: './login-form.component.html',
  styleUrls: ['./login-form.component.scss'],
  providers: [RootAdminService]
})
export class LoginFormComponent implements OnInit, OnDestroy {
  loginFormGroup: FormGroup;

  error: string;
  subscribtions: Subscription;

  constructor(
    private userService: RootAdminService,
    private router: Router,
    private store: Store,
    private jwtAuthService: JwtAuthService
  ) {
    this.subscribtions = new Subscription();

    this.loginFormGroup = new FormGroup({
      email: new FormControl('', [Validators.required]),
      password: new FormControl('', [
        Validators.required,
        Validators.minLength(8)
      ])
    });
  }

  /**
   * Hanlde submit on login form
   */
  public handleSubmit() {
    const { email, password } = this.loginFormGroup.value;
    const inputsValues: LoginForm = {
      email,
      password: sha1(password)
    };

    this.subscribtions.add(
      this.userService
        .sendLoginRequest(inputsValues)
        .subscribe(response => this.handleResponse(response))
    );
  }

  /**
   * Handle response
   * @param passedResponse UserData
   */
  private handleResponse(passedResponse) {
    const { token } = passedResponse;
    // Check if token and response with data exist
    if (token) {
      // Clear error message
      this.error = null;
      // Set future date

      const decodedToken = this.jwtAuthService.decodeToken(token);

      const { data } = decodedToken;
      // Setup localStorage with items;
      localStorage.setItem('token', token);

      if (token) {
        this.router.navigate(['dashboard/main']);

        this.store.dispatch(new AddUser(data));
      }
    } else {
      this.error = passedResponse.error;
    }
  }

  ngOnInit() {
    this.error = null;
  }

  ngOnDestroy() {
    this.subscribtions.unsubscribe();
  }
}
