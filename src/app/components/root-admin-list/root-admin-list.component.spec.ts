import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RootAdminListComponent } from './root-admin-list.component';

describe('RootAdminListComponent', () => {
  let component: RootAdminListComponent;
  let fixture: ComponentFixture<RootAdminListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RootAdminListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RootAdminListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
