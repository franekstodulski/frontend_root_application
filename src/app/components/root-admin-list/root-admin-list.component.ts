import { Component, OnInit, OnDestroy } from '@angular/core';
import { Subscription } from 'rxjs';
import { RootAdminService } from 'src/app/services/root-user/root-user.service';
import {
  FormGroup,
  FormControl,
  Validators
} from '../../../../node_modules/@angular/forms';
import * as sha1 from 'sha1/sha1';

@Component({
  selector: 'app-root-admin-list',
  templateUrl: './root-admin-list.component.html',
  styleUrls: ['./root-admin-list.component.scss'],
  providers: [RootAdminService]
})
export class RootAdminListComponent implements OnInit, OnDestroy {
  private subscriptions: Subscription;

  public newRootAdminForm: FormGroup;
  public rootAdminsList: any = [];
  public displayedColumns: string[];

  constructor(private rootAdminService: RootAdminService) {
    this.subscriptions = new Subscription();
  }

  ngOnInit() {
    this.displayedColumns = ['id', 'name', 'email', 'actions'];

    // Form
    this.newRootAdminForm = new FormGroup({
      name: new FormControl('', [Validators.required, Validators.minLength(3)]),
      password: new FormControl('', [
        Validators.required,
        Validators.minLength(8)
      ]),
      email: new FormControl('', [Validators.required, Validators.email])
    });

    this.subscriptions.add(
      this.rootAdminService
        .getAllRootAdmins()
        .subscribe(response => (this.rootAdminsList = response))
    );
  }
  /**
   * Remove user
   */
  public removeUser(userId) {
    if (this.rootAdminsList.length > 1) {
      const confirmPrompt = confirm('Are you sure?');
      if (confirmPrompt) {
        this.subscriptions.add(
          this.rootAdminService
            .removeRootAdmin(userId)
            .subscribe(res => (this.rootAdminsList = res))
        );
      }
    } else {
    }
  }

  public handleSubmit() {
    const { name, email, password } = this.newRootAdminForm.value;

    const newRootUser = {
      name,
      email,
      password: sha1(password)
    };

    this.rootAdminService.addNewRootAdmin(newRootUser).subscribe(response => {
      this.rootAdminsList = [...this.rootAdminsList, response];

      this.newRootAdminForm.reset();
    });
  }

  ngOnDestroy() {
    this.subscriptions.unsubscribe();
  }
}
