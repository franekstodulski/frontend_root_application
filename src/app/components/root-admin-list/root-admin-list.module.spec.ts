import { RootAdminListModule } from './root-admin-list.module';

describe('RootAdminListModule', () => {
  let rootAdminListModule: RootAdminListModule;

  beforeEach(() => {
    rootAdminListModule = new RootAdminListModule();
  });

  it('should create an instance', () => {
    expect(rootAdminListModule).toBeTruthy();
  });
});
