import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BoxDetailArticleComponent } from './box-detail-article.component';

describe('BoxDetailArticleComponent', () => {
  let component: BoxDetailArticleComponent;
  let fixture: ComponentFixture<BoxDetailArticleComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BoxDetailArticleComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BoxDetailArticleComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
