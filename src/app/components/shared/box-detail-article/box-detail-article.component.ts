import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-box-detail-article',
  templateUrl: './box-detail-article.component.html',
  styleUrls: ['./box-detail-article.component.scss']
})
export class BoxDetailArticleComponent implements OnInit {
  @Input()
  header: string;
  @Input()
  subheader: string;
  constructor() {}

  ngOnInit() {}
}
