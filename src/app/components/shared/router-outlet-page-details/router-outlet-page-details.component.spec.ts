import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RouterOutletPageDetailsComponent } from './router-outlet-page-details.component';

describe('RouterOutletPageDetailsComponent', () => {
  let component: RouterOutletPageDetailsComponent;
  let fixture: ComponentFixture<RouterOutletPageDetailsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RouterOutletPageDetailsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RouterOutletPageDetailsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
