import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-router-outlet-page-details',
  templateUrl: './router-outlet-page-details.component.html',
  styleUrls: ['./router-outlet-page-details.component.scss']
})
export class RouterOutletPageDetailsComponent implements OnInit {
  @Input()
  header: string;
  @Input()
  subheader: string;
  constructor() {}

  ngOnInit() {}
}
