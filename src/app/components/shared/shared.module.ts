import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterOutletPageDetailsComponent } from './router-outlet-page-details/router-outlet-page-details.component';
import { BoxDetailArticleComponent } from './box-detail-article/box-detail-article.component';
import { TableListComponent } from './table-list/table-list.component';

// Material
import { MatTableModule } from '@angular/material/table';

@NgModule({
  imports: [CommonModule, MatTableModule],
  declarations: [
    RouterOutletPageDetailsComponent,
    BoxDetailArticleComponent,
    TableListComponent
  ],
  exports: [
    RouterOutletPageDetailsComponent,
    BoxDetailArticleComponent,
    TableListComponent
  ]
})
export class SharedModule {}
