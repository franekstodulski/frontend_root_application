import { Component, OnInit } from '@angular/core';
import { Routes } from '@angular/router';
import { dashboardRoutes } from './../../views/dashboard-page/routes';
@Component({
  selector: 'app-sidebar-navigation',
  templateUrl: './sidebar-navigation.component.html',
  styleUrls: ['./sidebar-navigation.component.scss']
})
export class SidebarNavigationComponent implements OnInit {
  routes: Routes;
  constructor() {}

  ngOnInit() {
    this.routes = dashboardRoutes;
    console.log(this.routes);
  }
}
