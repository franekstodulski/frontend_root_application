import { SidebarNavigationModule } from './sidebar-navigation.module';

describe('SidebarNavigationModule', () => {
  let sidebarNavigationModule: SidebarNavigationModule;

  beforeEach(() => {
    sidebarNavigationModule = new SidebarNavigationModule();
  });

  it('should create an instance', () => {
    expect(sidebarNavigationModule).toBeTruthy();
  });
});
