import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { SidebarNavigationComponent } from './sidebar-navigation.component';

// Material
import { MatIconModule } from '@angular/material/icon';
import { MatListModule } from '@angular/material/list';

@NgModule({
  imports: [CommonModule, RouterModule, MatListModule, MatIconModule],
  declarations: [SidebarNavigationComponent],
  exports: [SidebarNavigationComponent]
})
export class SidebarNavigationModule {}
