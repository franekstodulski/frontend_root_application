import { Component, OnInit } from '@angular/core';
import { Select } from '@ngxs/store';
import { UserState } from '../../store/user.state';
import { Observable } from 'rxjs';
import { UserData } from '../../store/user.actions';
import { Router } from '@angular/router';

@Component({
  selector: 'app-top-navigation',
  templateUrl: './top-navigation.component.html',
  styleUrls: ['./top-navigation.component.scss']
})
export class TopNavigationComponent implements OnInit {
  @Select(UserState)
  userState$: Observable<UserData>;

  constructor(private router: Router) {}

  ngOnInit() {
    this.userState$.subscribe(res => console.log(res));
  }

  logout() {
    localStorage.removeItem('userDetails');
    // Navigate to login page
    this.router.navigate(['login']);
  }
}
