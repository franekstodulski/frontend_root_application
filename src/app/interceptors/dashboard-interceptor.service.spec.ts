import { TestBed } from '@angular/core/testing';

import { DashboardInterceptorService } from './dashboard-interceptor.service';

describe('DashboardInterceptorService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: DashboardInterceptorService = TestBed.get(DashboardInterceptorService);
    expect(service).toBeTruthy();
  });
});
