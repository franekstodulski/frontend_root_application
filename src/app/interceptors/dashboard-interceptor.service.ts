import { Observable } from 'rxjs';
import { Injectable } from '@angular/core';

import {
  HttpEvent,
  HttpInterceptor,
  HttpHandler,
  HttpRequest
} from '@angular/common/http';
import { JwtAuthService } from '../services/jwt-auth/jwt-auth.service';

@Injectable()
export class DashboardInterceptorService implements HttpInterceptor {
  constructor(private jwtAuthService: JwtAuthService) {}

  intercept(
    request: HttpRequest<any>,
    next: HttpHandler
  ): Observable<HttpEvent<any>> {
    const token = localStorage.getItem('token');

    if (token && !this.jwtAuthService.tokenIsExpired(token)) {
      const clonedReq = request.clone({
        setHeaders: {
          token,
          'Content-Type': 'application/json'
        }
      });

      return next.handle(clonedReq);
    } else {
      return next.handle(request);
    }
  }
}
