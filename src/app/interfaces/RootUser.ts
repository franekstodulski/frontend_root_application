export interface IRootUser {
  id: number;
  email: string;
  name: string;
}
