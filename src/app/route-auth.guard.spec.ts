import { TestBed, async, inject } from '@angular/core/testing';

import { RouteAuthGuard } from './route-auth.guard';

describe('RouteAuthGuard', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [RouteAuthGuard]
    });
  });

  it('should ...', inject([RouteAuthGuard], (guard: RouteAuthGuard) => {
    expect(guard).toBeTruthy();
  }));
});
