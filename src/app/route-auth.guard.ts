import { Injectable } from '@angular/core';
import {
  CanActivate,
  ActivatedRouteSnapshot,
  RouterStateSnapshot,
  Router
} from '@angular/router';
import { JwtAuthService } from './services/jwt-auth/jwt-auth.service';

@Injectable({
  providedIn: 'root'
})
export class RouteAuthGuard implements CanActivate {
  constructor(private router: Router, private jwtAuthService: JwtAuthService) {}
  canActivate(
    next: ActivatedRouteSnapshot,
    state: RouterStateSnapshot
  ): boolean {
    const token = localStorage.getItem('token');

    if (token) {
      if (token && !this.jwtAuthService.tokenIsExpired(token)) {
        return true;
      } else {
        // clean up localstorage
        localStorage.removeItem('userDetails');
        // Navigate to login page
        this.router.navigate(['login']);

        return false;
      }
    } else {
      this.router.navigate(['login']);

      return false;
    }
  }
}
