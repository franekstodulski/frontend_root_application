import { Routes } from '@angular/router';
import { LoginPageComponent } from './views/login-page/login-page.component';
import { NotFoundPageComponent } from './views/not-found-page/not-found-page.component';
import { DashboardPageComponent } from './views/dashboard-page/dashboard-page.component';
// Guard
import { RouteAuthGuard } from './route-auth.guard';

export const routes: Routes = [
  {
    path: '',
    redirectTo: '/login',
    pathMatch: 'full'
  },
  {
    path: 'login',
    component: LoginPageComponent
  },
  {
    path: 'dashboard',
    component: DashboardPageComponent,
    canActivate: [RouteAuthGuard]
  },
  {
    path: '**',
    component: NotFoundPageComponent
  }
];
