import { Injectable } from '@angular/core';
import { JwtHelperService } from '@auth0/angular-jwt';

@Injectable({
  providedIn: 'root'
})
export class JwtAuthService {
  private jwtHelper: JwtHelperService;
  constructor() {
    this.jwtHelper = new JwtHelperService();
  }

  public decodeToken(token) {
    return this.jwtHelper.decodeToken(token);
  }

  public tokenIsExpired(token) {
    return this.jwtHelper.isTokenExpired(token);
  }

  public getTokenExpirationDate(token) {
    return this.jwtHelper.getTokenExpirationDate(token);
  }
}
