import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';

// Interface
import { LoginForm } from '../../components/login-form/login-form';

// Environment variable
import { environment } from '../../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class RootAdminService {
  constructor(private http: HttpClient) {}

  sendLoginRequest(loginValues: LoginForm) {
    return this.http.post(
      environment.hostURL.public + '/root_admin/login',
      loginValues
    );
  }

  addNewRootAdmin(newRootUser) {
    return this.http.post(
      environment.hostURL.secured + '/root_admin/add',
      newRootUser
    );
  }

  getRootAdminDetails(id: number) {
    return this.http.get(
      environment.hostURL.secured + '/root_admin/details/' + id
    );
  }

  getAllRootAdmins() {
    return this.http.get(environment.hostURL.secured + '/root_admin/all');
  }

  removeRootAdmin(id) {
    return this.http.delete(
      environment.hostURL.secured + '/root_admin/delete/' + id
    );
  }
}
