export interface UserData {
  id: number;
  name: string;
  email: string;
}

export class AddUser {
  static readonly type = '[User] Add User Data';
  constructor(public userData: UserData) {}
}
