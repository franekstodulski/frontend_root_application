import { Action, State } from '@ngxs/store';
import { AddUser, UserData } from './user.actions';

@State<UserData>({
  name: 'user',
  defaults: {
    id: null,
    name: '',
    email: ''
  }
})
export class UserState {
  @Action(AddUser)
  add({ setState }, action: AddUser) {
    const { userData } = action;

    setState(userData);
  }
}
