import { Component, OnInit } from '@angular/core';
import { Subscription } from 'rxjs';
import { Store } from '@ngxs/store';
import { AddUser } from '../../store/user.actions';
import { RootAdminService } from '../../services/root-user/root-user.service';
import { JwtAuthService } from 'src/app/services/jwt-auth/jwt-auth.service';

@Component({
  selector: 'app-dashboard-page',
  templateUrl: './dashboard-page.component.html',
  styleUrls: ['./dashboard-page.component.scss']
})
export class DashboardPageComponent implements OnInit {
  // Create class to gather all subscriptions;
  private subscriptions: Subscription;

  constructor(
    private rootAdminService: RootAdminService,
    private store: Store,
    private jwtauthService: JwtAuthService
  ) {
    this.subscriptions = new Subscription();
  }

  ngOnInit() {
    this.fetchUserData();
  }

  /**
   * Fetch user details if logged in
   * Case: refresh page
   */
  private fetchUserData() {
    const token = localStorage.getItem('token');

    const decodedToken = this.jwtauthService.decodeToken(token);

    this.subscriptions.add(
      this.rootAdminService
        .getRootAdminDetails(decodedToken.data.id)
        .subscribe((userDetails: any) => {
          if (userDetails) {
            this.store.dispatch(new AddUser(userDetails));
          }
        })
    );
  }
}
