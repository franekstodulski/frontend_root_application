import { DashboardInterceptorService } from './../../interceptors/dashboard-interceptor.service';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';

// Modules
import { ViewsModule } from './views/views.module';
import { SidebarNavigationModule } from '../../components/sidebar-navigation/sidebar-navigation.module';
import { TopNavigationModule } from '../../components/top-navigation/top-navigation.module';

// Components
import { DashboardPageComponent } from './dashboard-page.component';

// Routes
import { routes } from './routes';

@NgModule({
  imports: [
    CommonModule,
    ViewsModule,
    HttpClientModule,
    RouterModule.forChild(routes),
    SidebarNavigationModule,
    TopNavigationModule
  ],
  declarations: [DashboardPageComponent],
  exports: [DashboardPageComponent],
  providers: [
    {
      provide: HTTP_INTERCEPTORS,
      useClass: DashboardInterceptorService,
      multi: true
    }
  ]
})
export class DashboardPageModule {}
