import { Routes, Route } from '@angular/router';
import { DashboardPageComponent } from './dashboard-page.component';
import { ProductsPageComponent } from './views/products-page/products-page.component';
import { MainPageComponent } from './views/main-page/main-page.component';
import { TrainersPageComponent } from './views/trainers-page/trainers-page.component';
import { StudentsPageComponent } from './views/students-page/students-page.component';
import { RootAdminPageComponent } from './views/root-admin-page/root-admin-page.component';
import { DishesPageComponent } from './views/dishes-page/dishes-page.component';
import { RouteAuthGuard } from '../../route-auth.guard';

export const routes: Routes = [
  {
    path: 'dashboard',
    component: DashboardPageComponent,
    canActivate: [RouteAuthGuard],
    children: [
      {
        path: 'main',
        component: MainPageComponent,
        data: {
          svgIcon: 'home'
        }
      },
      {
        path: 'products',
        component: ProductsPageComponent,
        data: {
          svgIcon: 'fastfood'
        }
      },
      {
        path: 'dishes',
        component: DishesPageComponent,
        data: {
          svgIcon: 'local_dining'
        }
      },
      {
        path: 'trainers',
        component: TrainersPageComponent,
        data: {
          svgIcon: 'face'
        }
      },
      {
        path: 'students',
        component: StudentsPageComponent,
        data: {
          svgIcon: 'people'
        }
      },
      {
        path: 'root-admins',
        component: RootAdminPageComponent,
        data: {
          svgIcon: 'fingerprint'
        }
      }
    ]
  }
];

export const dashboardRoutes = routes.reduce((acc, value) => {
  const routeArray = value.children.map(child => {
    return {
      path: '/' + value.path + '/' + child.path,
      label: child.path,
      svgIcon: child.data.svgIcon
    };
  });

  return routeArray;
}, []);
