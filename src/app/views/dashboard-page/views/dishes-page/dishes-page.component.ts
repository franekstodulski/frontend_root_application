import { Component, OnInit } from '@angular/core';
import { DishesService } from 'src/app/services/dishes/dishes.service';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-dishes-page',
  templateUrl: './dishes-page.component.html',
  styleUrls: ['./dishes-page.component.scss']
})
export class DishesPageComponent implements OnInit {
  private subscriptions: Subscription;

  constructor(private dishesService: DishesService) {
    this.subscriptions = new Subscription();
  }

  ngOnInit() {
    this.subscriptions.add(
      this.dishesService
        .getAllDishes()
        .subscribe(response => console.log(response))
    );
  }
}
