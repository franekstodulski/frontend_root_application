import { DishesPageModule } from './dishes-page.module';

describe('DishesPageModule', () => {
  let dishesPageModule: DishesPageModule;

  beforeEach(() => {
    dishesPageModule = new DishesPageModule();
  });

  it('should create an instance', () => {
    expect(dishesPageModule).toBeTruthy();
  });
});
