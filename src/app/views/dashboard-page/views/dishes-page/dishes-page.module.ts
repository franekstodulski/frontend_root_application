import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { DishesPageComponent } from './dishes-page.component';
import { SharedModule } from 'src/app/components/shared/shared.module';
import { DishesService } from 'src/app/services/dishes/dishes.service';

@NgModule({
  imports: [CommonModule, SharedModule],
  declarations: [DishesPageComponent],
  providers: [DishesService]
})
export class DishesPageModule {}
