import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MainPageComponent } from './main-page.component';
import { SharedModule } from '../../../../components/shared/shared.module';

@NgModule({
  imports: [CommonModule, SharedModule],
  declarations: [MainPageComponent]
})
export class MainPageModule {}
