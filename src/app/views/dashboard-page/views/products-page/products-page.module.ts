import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ProductsPageComponent } from './products-page.component';
import { SharedModule } from '../../../../components/shared/shared.module';

@NgModule({
  imports: [CommonModule, SharedModule],
  declarations: [ProductsPageComponent]
})
export class ProductsPageModule {}
