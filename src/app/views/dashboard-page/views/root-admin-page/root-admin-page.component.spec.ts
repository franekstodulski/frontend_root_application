import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RootAdminPageComponent } from './root-admin-page.component';

describe('RootAdminPageComponent', () => {
  let component: RootAdminPageComponent;
  let fixture: ComponentFixture<RootAdminPageComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RootAdminPageComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RootAdminPageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
