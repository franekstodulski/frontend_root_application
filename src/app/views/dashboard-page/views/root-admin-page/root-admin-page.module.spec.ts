import { RootAdminPageModule } from './root-admin-page.module';

describe('RootAdminPageModule', () => {
  let rootAdminPageModule: RootAdminPageModule;

  beforeEach(() => {
    rootAdminPageModule = new RootAdminPageModule();
  });

  it('should create an instance', () => {
    expect(rootAdminPageModule).toBeTruthy();
  });
});
