import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RootAdminPageComponent } from './root-admin-page.component';

// 3rd party
import { RootAdminListModule } from 'src/app/components/root-admin-list/root-admin-list.module';
import { SharedModule } from 'src/app/components/shared/shared.module';

@NgModule({
  imports: [CommonModule, SharedModule, RootAdminListModule],
  declarations: [RootAdminPageComponent]
})
export class RootAdminPageModule {}
