import { StudentsPageModule } from './students-page.module';

describe('StudentsPageModule', () => {
  let studentsPageModule: StudentsPageModule;

  beforeEach(() => {
    studentsPageModule = new StudentsPageModule();
  });

  it('should create an instance', () => {
    expect(studentsPageModule).toBeTruthy();
  });
});
