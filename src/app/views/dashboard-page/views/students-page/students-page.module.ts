import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { StudentsPageComponent } from './students-page.component';
import { SharedModule } from '../../../../components/shared/shared.module';

@NgModule({
  imports: [CommonModule, SharedModule],
  declarations: [StudentsPageComponent]
})
export class StudentsPageModule {}
