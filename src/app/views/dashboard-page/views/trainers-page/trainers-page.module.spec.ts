import { TrainersPageModule } from './trainers-page.module';

describe('TrainersPageModule', () => {
  let trainersPageModule: TrainersPageModule;

  beforeEach(() => {
    trainersPageModule = new TrainersPageModule();
  });

  it('should create an instance', () => {
    expect(trainersPageModule).toBeTruthy();
  });
});
