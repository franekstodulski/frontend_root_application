import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TrainersPageComponent } from './trainers-page.component';
import { SharedModule } from '../../../../components/shared/shared.module';

@NgModule({
  imports: [CommonModule, SharedModule],
  declarations: [TrainersPageComponent]
})
export class TrainersPageModule {}
