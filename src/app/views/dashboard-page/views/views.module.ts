import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ProductsPageModule } from './products-page/products-page.module';
import { MainPageModule } from './main-page/main-page.module';
import { TrainersPageModule } from './trainers-page/trainers-page.module';
import { StudentsPageModule } from './students-page/students-page.module';
import { RootAdminPageModule } from './root-admin-page/root-admin-page.module';
import { DishesPageModule } from './dishes-page/dishes-page.module';

@NgModule({
  imports: [
    CommonModule,
    ProductsPageModule,
    MainPageModule,
    TrainersPageModule,
    StudentsPageModule,
    RootAdminPageModule,
    DishesPageModule
  ],
  declarations: [],
  exports: [
    ProductsPageModule,
    MainPageModule,
    TrainersPageModule,
    StudentsPageModule,
    RootAdminPageModule,
    DishesPageModule
  ]
})
export class ViewsModule {}
