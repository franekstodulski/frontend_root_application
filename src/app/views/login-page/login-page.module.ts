import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { LoginPageComponent } from './login-page.component';
// Login Form
import { LoginFormModule } from '../../components/login-form/login-form.module';

@NgModule({
  imports: [CommonModule, LoginFormModule],
  declarations: [LoginPageComponent]
})
export class LoginPageModule {}
