import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { LoginPageModule } from './login-page/login-page.module';
import { NotFoundPageModule } from './not-found-page/not-found-page.module';
import { DashboardPageModule } from './dashboard-page/dashboard-page.module';

@NgModule({
  imports: [
    CommonModule,
    LoginPageModule,
    NotFoundPageModule,
    DashboardPageModule
  ],
  declarations: [],
  exports: [
    LoginPageModule,
    NotFoundPageModule,
    DashboardPageModule,
    DashboardPageModule
  ]
})
export class ViewsModule {}
